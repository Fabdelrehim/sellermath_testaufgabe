#csv_uploader/urls.py
from django.urls import path
from .views import report_upload, database_view, database_two_columns_view_sorted_by_address, database_two_columns_view_sorted_by_price

urlpatterns = [      
    path('', report_upload, name="report_upload"),
    path('database_complete_view/', database_view, name="database_complete_view"),
    path('database_two_columns_view_sorted_by_address/', database_two_columns_view_sorted_by_address, name="database_two_columns_view_sorted_by_address"),    
    path('database_two_columns_view_sorted_by_price/', database_two_columns_view_sorted_by_price, name="database_two_columns_view_sorted_by_price")    
]