import csv
import io 
from django.shortcuts import render
from django.contrib import messages
from django.contrib.auth.decorators import permission_required
from django.db.models import Subquery
from .models import Report

# Import for string to datetime conversion
from datetime import datetime 

# Only Admin can uplad csv-files
# @permission_required('admin.can_add_log_entry')

# Create function-based-view
def report_upload(request):
    # Define template
    template = 'report_upload.html'

    # Give user information about the csv 
    prompt = {
        'order': 'Bitte wählen Sie eine gültige .csv-Datei zum hochladen aus.'
    }

    # GET-Request-Handler
    if request.method == 'GET':
        return render(request, template, prompt)

    # Grab the file from form
    csv_file = request.FILES['file']

    # Ensure correct file was uploaded (.csv)
    if not csv_file.name.endswith('.csv'):
        messages.error(request, 'Das war keine gültige Datei!')
    else: 
        messages.success(request, 'Hochladen Erfolgreich!')
        # Read csv-files & Convert bytes to string and ignore errors
        data_set = str(csv_file.read(), errors='ignore')

        # Put the data_set into a stream
        io_string = io.StringIO(data_set)

        # Skip the header
        next(io_string)

        # Loop through csv-file (change quotechar to pipe symbol)
        for column in csv.reader(io_string, delimiter=';', quotechar="|"):

            # Save columnwise (semicolon-separated)
            _, created = Report.objects.update_or_create(
                order_number_idealo = column[0],
                order_number_shop = column[1],	
                created_at = column[2],		
                processed_at = column[3],		
                status = column[4],		
                currency = column[5],		
                total_price_with_shipping = column[6],	
                total_price_without_shipping = column[7],	
                total_shipping = column[8],	
                customer_email = column[9],		
                customer_phone = column[10],		
                shipping_address_address1 = column[11],		
                shipping_address_address2 = column[12],		
                shipping_address_city = column[13],		
                shipping_address_country = column[14],		
                shipping_address_given_name = column[15],		
                shipping_address_family_name = column[16],		
                shipping_address_zip = column[17],		
                shipping_address_salutation = column[18],	
                billing_address_address1 = column[19],		
                billing_address_address2 = column[20],		
                billing_address_city = column[21],		
                billing_address_country = column[22],		
                billing_address_given_name = column[23],		
                billing_address_family_name = column[24],		
                billing_address_zip = column[25],		
                billing_address_salutation = column[26],		
                fulfillment_carrier = column[27],		
                fulfillment_type = column[28],		
                fulfillment_transaction_code = column[29],		
                fulfillment_options = column[30],		
                payment_method = column[31],		
                payment_transaction_id = column[32],		
                merchant_id = column[33],		
                merchant_name = column[34],		
                total_item_price = column[35],		
                item_price = column[36],	
                former_price = column[37],	
                price_range_amount = column[38],		
                quantity = column[39],	
                sku = column[40],		
                title = column[41],		
                delivery_time = column[42],		
                voucher_code = column[43],		
                refund_ids = column[44],		
                refund_amounts = column[45],	
                refund_statuses = column[46],	
            )

    # Return empty context
    context = {}

    return render(request, template, context)




# Full Database View
def database_view(request):
    # Define template
    template = 'database_complete_view.html'

    # GET-Request-Handler    
    if request.method == 'GET':
        # Query all entries
        objs = Report.objects.all() 
        context =  {'objs':objs}
        return render(request, template, context)


# Sparse Database View (sorted by address)
def database_two_columns_view_sorted_by_address(request):
    # Define template
    template = 'database_two_columns_view_sorted_by_address.html'

    # GET-Request-Handler    
    if request.method == 'GET':

        # Query only two columns
        objs = Report.objects.values('shipping_address_zip','total_item_price').order_by('shipping_address_zip')
        context =  {'objs':objs}

        return render(request, template, context)

# Sparse Database View (sorted by price)
def database_two_columns_view_sorted_by_price(request):
    # Define template
    template = 'database_two_columns_view_sorted_by_price.html'

    # GET-Request-Handler    
    if request.method == 'GET':

        # Query only two columns
        objs = Report.objects.values('shipping_address_zip','total_item_price').order_by('total_item_price')
        context =  {'objs':objs}

        return render(request, template, context)    